import { Builder, By, Key, until, Capabilities } from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome";
import { path } from "chromedriver";

let service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
var driver = new Builder().withCapabilities(Capabilities.chrome()).build();

const login_url = "http://admin-dev.styletribute.com/login";

jest.setTimeout(40000);

beforeEach(async () => {
    await driver.get(login_url);
    await driver.wait(until.elementLocated(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")), 10000);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys("admin@styletribute.com");
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys("secret",Key.RETURN);
  });
describe("addnewproduct", async() => {
    it("add new job success", async() => {
    await driver.wait(until.elementLocated(By.css("#sidebar-menu > div > ul > li:nth-child(2) > a")),20000);
    await driver.findElement(By.css("#sidebar-menu > div > ul > li:nth-child(2) > a")).click();
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-list > div > div > div > div:nth-child(2) > div.form-inline.table-meta > div.col-right.text-right > a")).click();
    await driver.wait(until.elementLocated(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(1) > input")),10000);
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(1) > input")).sendKeys("Product test 1");
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(2) > input")).sendKeys("400");
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(3) > input")).sendKeys("950");
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(4) > select")).click();
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(4) > select > option:nth-child(3)")).click();
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(6) > select > option:nth-child(2)")).click();
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(7) > select > option:nth-child(2)")).click();
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(8) > select > option:nth-child(2)")).click();
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(9) > select > option:nth-child(2)")).click();
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(11) > select > option:nth-child(7)")).click();
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(17) > input")).sendKeys("red");
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(18) > input")).sendKeys("leather");
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(19) > textarea")).sendKeys("Test Descriptions");
    await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(20) > input")).click();
    })
    // it("don't select dropdown", async() => {
    // await driver.wait(until.elementLocated(By.css("#sidebar-menu > div > ul > li:nth-child(2) > a")),30000);
    // await driver.findElement(By.css("#sidebar-menu > div > ul > li:nth-child(2) > a")).click();
    // await driver.findElement(By.css("#layout > div.right_col > app-product > app-list > div > div > div > div:nth-child(2) > div.form-inline.table-meta > div.col-right.text-right > a")).click();
    // await driver.wait(until.elementLocated(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(1) > input")),20000);
    // await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(1) > input")).sendKeys("Product test 1");
    // await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(2) > input")).sendKeys("400");
    // await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(3) > input")).sendKeys("950");
    // await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(17) > input")).sendKeys("red");
    // await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(18) > input")).sendKeys("leather");
    // await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(19) > textarea")).sendKeys("Test Descriptions");
    // await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(20) > input")).click();
    // let message1 = await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(4) > ul > li")).getText();
    // expect("This field is required.").toEqual(message1);
    // let message2 = await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(5) > ul > li")).getText();
    // expect("This field is required.").toEqual(message2);
    // let message3 = await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(6) > ul > li")).getText();
    // expect("This field is required.").toEqual(message3);
    // let message4 = await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(7) > ul > li")).getText();
    // expect("This field is required.").toEqual(message4);
    // let message5 = await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(8) > ul > li")).getText();
    // expect("This field is required.").toEqual(message5);
    // let message6 = await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(9) > ul > li")).getText();
    // expect("This field is required.").toEqual(message6);
    // let message7 = await driver.findElement(By.css("#layout > div.right_col > app-product > app-create > div > div:nth-child(2) > div > form > div:nth-child(11) > ul > li")).getText();
    // expect("This field is required.").toEqual(message7);
    // })
    
})