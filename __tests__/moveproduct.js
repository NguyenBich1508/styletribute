import { Builder, By, Key, until, Capabilities } from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome";
import { path } from "chromedriver";

let service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
var driver = new Builder().withCapabilities(Capabilities.chrome()).build();

const login_url = "http://admin-dev.styletribute.com/login";

jest.setTimeout(40000);

beforeEach(async () => {
    await driver.get(login_url);
    await driver.wait(until.elementLocated(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")), 20000);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys("admin@styletribute.com");
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys("secret", Key.RETURN);
});
describe("move product", async () => {
    it("move to destination SG1 & Rack 1.2", async () => {
        await driver.wait(until.elementLocated(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(4) > table > tbody > tr:nth-child(1) > td:nth-child(3)")), 20000);
      
        await driver.findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(4) > table > tbody > tr:nth-child(1) > td:nth-child(5) > select")).sendKeys("SG1");
        await driver.findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(4) > table > tbody > tr:nth-child(1) > td:nth-child(6) > select")).sendKeys("RACK 1-2");
        await driver.findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(4) > table > tbody > tr:nth-child(1) > td:nth-child(7) > button")).click();
        await driver.sleep(1000);
        let messsge = await driver.findElement(By.css("#nf-notify > div > span")).getText();
        expect("Product location is updated").toEqual(messsge);
    })
    
})
