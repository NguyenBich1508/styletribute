import { Builder, By, Key, until, Capabilities } from "selenium-webdriver";

import chrome from "selenium-webdriver/chrome";
import { path } from "chromedriver";

let service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
var driver = new Builder().withCapabilities(Capabilities.chrome()).build();

const login_url = "http://admin-dev.styletribute.com/login";

jest.setTimeout(20000);
beforeEach(async () => {
    await driver.get(login_url);
    await driver.wait(until.elementLocated(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")), 10000);
  });
test("login fail when password wrong", async() => {
    let mock_user = {id: 1, email: "admin@styletribute.com", password: "secret1", expected_message: "Email or password does not match."};
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys(mock_user.email);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys(mock_user.password, Key.RETURN);
    await driver.wait(until.elementsLocated(By.css("#nf-notify > div > span")));
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect(mock_user.expected_message).toEqual(message);
  });
test("login fail when email wrong", async() => {
    let mock_user = {id: 2, email: "admin@styletribute", password: "secret", expected_message: "Email or password does not match."};
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys(mock_user.email);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys(mock_user.password, Key.RETURN);
    await driver.wait(until.elementsLocated(By.css("#nf-notify > div > span")));
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect(mock_user.expected_message).toEqual(message);
  }); 
test("login fail when email invalid & password wrong", async() => {
    let mock_user = {id: 3, email: "admin@styletribute", password: "secret1", expected_message: "Email or password does not match."};
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys(mock_user.email);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys(mock_user.password, Key.RETURN);
    await driver.wait(until.elementsLocated(By.css("#nf-notify > div > span")));
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect(mock_user.expected_message).toEqual(message);
  });
test("login fail when email empty & password true", async() => {
    let mock_user = {id: 4, email: "", password: "secret", expected_message: "Email or password does not match."};
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys(mock_user.email);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys(mock_user.password, Key.RETURN);
    await driver.wait(until.elementsLocated(By.css("#nf-notify > div > span")));
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect(mock_user.expected_message).toEqual(message);
});
test("login fail when email true & password empty", async() => {
    let mock_user = {id: 5, email: "admin@styletribute.com", password: "", expected_message: "Email or password does not match."};
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys(mock_user.email);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys(mock_user.password, Key.RETURN);
    await driver.wait(until.elementsLocated(By.css("#nf-notify > div > span")));
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect(mock_user.expected_message).toEqual(message);
});
test("login fail when email&password empty", async() => {
    let mock_user = {id: 6, email: "", password: "", expected_message: "Email or password does not match."};
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys(mock_user.email);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys(mock_user.password, Key.RETURN);
    await driver.wait(until.elementsLocated(By.css("#nf-notify > div > span")));
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect(mock_user.expected_message).toEqual(message);
});
test("login success", async() => {
    let mock_user = {id: 7, email: "admin@styletribute.com", password: "secret", expected_message: "Login Success"};
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys(mock_user.email);
    await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys(mock_user.password, Key.RETURN);
    await driver.wait(until.elementsLocated(By.css("#nf-notify > div > span")));
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect(mock_user.expected_message).toEqual(message);
});
