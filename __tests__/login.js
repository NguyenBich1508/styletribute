import { Builder, By, Key, until, Capabilities } from "selenium-webdriver";

import chrome from "selenium-webdriver/chrome";
import { path } from "chromedriver";

let service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
var driver = new Builder().withCapabilities(Capabilities.chrome()).build();

const login_url = "http://admin-dev.styletribute.com/login";

jest.setTimeout(20000);

let TestCases = [
  //{ testcase: "should reject bad credentials", id: 2, email: "abc@abc.com", password: "secret", expected_message: "Email or password does not match." },
  //{ testcase: "reject", id: 3, email: "", password: "", expected_message: "Email or password does not match." },
  { testcase: "should accept correct credentials", id: 1, email: "admin@styletribute.com", password: "secret", expected_message: "Login Success" },
];

beforeEach(async () => {
  await driver.get(login_url);
  await driver.wait(until.elementLocated(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")), 10000);
});

describe("Login", () => {
  TestCases.forEach(item => {
    it(item.testcase, async () => {
      await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys(item.email);
      await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys(item.password, Key.RETURN);
      await driver.wait(until.elementsLocated(By.css("#nf-notify > div > span")));
      let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
      expect(item.expected_message).toEqual(message);
    });
  });
});