import { Builder, By, Key, until, Capabilities } from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome";
import { path } from "chromedriver";
import { Contain } from "../Utils/Helpers";

let service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
var driver = new Builder().withCapabilities(Capabilities.chrome()).build();

const user_page_url = "http://beppo-admin.vicoders.com/user";
const login_url = "http://beppo-admin.vicoders.com/login";

jest.setTimeout(40000);

beforeEach(async () => {
    await driver.get(login_url);
    await driver.wait(until.elementLocated(By.name("email")), 20000);
    await driver.findElement(By.name("email")).sendKeys("superadmin@beppo.com");
    await driver.findElement(By.name("password")).sendKeys("secret", Key.RETURN);
    await driver.wait(
      until.elementLocated(By.name("keyword")),
      10000
    );
  });

describe("Testscript delete Account", async() => {
  it("able to delete destination", async() => {
    await driver.get(user_page_url);
    await driver.wait(
      until.elementLocated(By.css("#layout > div.right_col > app-user > app-list > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(4) > button")),
      10000
    );
    let tablerows = driver.findElements(By.xpath("//table/tbody/tr"))
        .then(function (elements) {
        console.log("total number of rows:"+ elements.length);
        let k = elements.length;
        for(let j = 1; j < k+1; j++){
        let valuetext = driver.findElement(By.css("#layout > div.right_col > app-user > app-list > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child("+j+") > td:nth-child(2)"))
        .getText().then(function(text){
          if(text == "bichnt@vicoders.com"){
            driver.findElement(By.css("#layout > div.right_col > app-user > app-list > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child("+j+") > td:nth-child(4) > button"))
          .click().then(function(){
            driver.findElement(By.css("body > div > div > div.swal2-actions > button.swal2-confirm.swal2-styled")).click();
            //confirm delete account:----body > div > div > div.swal2-actions > button.swal2-cancel.swal2-styled
            // driver.wait(until.alertIsPresent());
            // driver.switchTo().alert().accept(); 
          })
          }
        }) 
      }
    });
  })
})