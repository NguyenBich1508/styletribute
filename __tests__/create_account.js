import { Builder, By, Key, until, Capabilities } from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome";
import { path } from "chromedriver";
import { Contain } from "../Utils/Helpers";

let service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
var driver = new Builder().withCapabilities(Capabilities.chrome()).build();

const create_page_url = "http://beppo.vicoders.com/my-account/create";
const customer_information_page_url = "http://beppo.vicoders.com/my-account/information";
jest.setTimeout(40000);

beforeEach(async () => {
    await driver.get(create_page_url);
    await driver.wait(
      until.elementLocated(By.name("first_name")),
      10000
    );
  });

describe("Testscript Create Account", async() => {
  it("don't enter all field", async() => {
    let value = ["1","2","3","4"];
    await driver.findElement(By.name("first_name")).sendKeys("");
    await driver.findElement(By.name("last_name")).sendKeys("");
    await driver.findElement(By.name("email")).sendKeys("");
    await driver.findElement(By.name("password")).sendKeys("");
    await driver.findElement(By.css("button[type='submit']")).submit();
    for(const i in value){
      let message = await driver.findElement(By.css("body > app-root > app-my-account > app-create > div > div > form > div:nth-child("+value[i]+") > div > p"))
          .getText();
      if(message == "Required"){
          return true;
      }
      else return false;
  }
  })
//----------------------------------------------------------------------------------------------------------------
  it("email exist", async() => {
    await driver.findElement(By.name("first_name")).sendKeys("Nguyễn");
    await driver.findElement(By.name("last_name")).sendKeys("Bích");
    await driver.findElement(By.name("email")).sendKeys("superadmin@beppo.com");
    await driver.findElement(By.name("password")).sendKeys("27062004");
    await driver.findElement(By.css("button[type='submit']")).submit();
    await driver.sleep(2000);
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect("User already exists").toEqual(message);
  })
//-------------------------------------------------------------------------------------------------------------------
  it("invalid email", async() => {
    await driver.findElement(By.name("first_name")).sendKeys("Nguyễn");
    await driver.findElement(By.name("last_name")).sendKeys("Bích");
    await driver.findElement(By.name("email")).sendKeys("admin@beppo");
    await driver.findElement(By.name("password")).sendKeys("27062004");
    await driver.findElement(By.css("button[type='submit']")).submit();
    await driver.sleep(2000);
    let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
    expect("The email must be a valid email address.").toEqual(message);
  })
//---------------------------------------------------------------------------------------------------------------------
  it("Create successfully", async() =>{
    let ran = 0;
    ran = Math.floor((Math.random() * 100) + 1);
    await driver.findElement(By.name("first_name")).sendKeys("Nguyễn");
    await driver.findElement(By.name("last_name")).sendKeys("Bích");
    await driver.findElement(By.name("email")).sendKeys("bichnt" + ran + "@vicoders.com");
    await driver.findElement(By.name("password")).sendKeys("27062004");
    await driver.findElement(By.css("button[type='submit']")).submit();
    await driver.sleep(2000);
    let link = await driver.getCurrentUrl();
    expect(link).toMatch(customer_information_page_url);
  })
  })