import { Builder, By, Key, until, Capabilities } from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome";
import { path } from "chromedriver";
import { Contain } from "../Utils/Helpers";

let service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
var driver = new Builder().withCapabilities(Capabilities.chrome()).build();

const login_url = "http://beppo.vicoders.com/my-account/login";
const customer_information_page_url = "http://beppo.vicoders.com/my-account/information";

jest.setTimeout(40000);

beforeEach(async () => {
    await driver.get(login_url);
})

// afterEach(async () => {
//     await driver.findElement(By.css("body > app-root > app-my-account > app-information > div > div > div.button-group > div > div:nth-child(1) > a")).click();
// })

describe("Script login", async() => {
    it("invalid email", async() => {
        await driver.findElement(By.name("email")).sendKeys("bichnt@vicoders");
        await driver.findElement(By.name("password")).sendKeys("27062004", Key.RETURN);
        await driver.sleep(2000);
        let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
        expect("User not found").toEqual(message);
    })
//----------------------------------------------------------------------------------------------
    it("invalid password", async() => {
        await driver.findElement(By.name("email")).sendKeys("bichnt@vicoders.com");
        await driver.findElement(By.name("password")).sendKeys("270", Key.RETURN);
        await driver.sleep(2000);
        let message = await driver.findElement(By.css("#nf-notify > div > span")).getText();
        expect("Password does not match").toEqual(message);
    })
//-------------------------------------------------------------------------------------------------
    it("empty all fields", async() => {
        let value = ["1","2"];
        await driver.findElement(By.name("email")).sendKeys("");
        await driver.findElement(By.name("password")).sendKeys("", Key.RETURN);
        for(const i in value){
            let message = await driver.findElement(By.css("body > app-root > app-my-account > app-login > div > div > form > div:nth-child("+value[i]+") > div > p"))
                .getText();
            if(message == "Required"){
                return true;
            }
            else return false;
        }
    })
//-----------------------------------------------------------------------------------------------------
    it("log in successfully", async() => {
        await driver.findElement(By.name("email")).sendKeys("bichnt@vicoders.com");
        await driver.findElement(By.name("password")).sendKeys("27062004", Key.RETURN);
        await driver.sleep(2000);
        let link = await driver.getCurrentUrl();
        expect(link).toMatch(customer_information_page_url);
    })
    
})