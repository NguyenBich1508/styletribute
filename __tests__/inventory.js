import { Builder, By, Key, until, Capabilities } from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome";
import { path } from "chromedriver";
import { Contain } from "../Utils/Helpers";

let service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
var driver = new Builder().withCapabilities(Capabilities.chrome()).build();

const base_url = "http://admin-dev.styletribute.com";
const login_url = "http://admin-dev.styletribute.com/login";
const product_page_url = "http://admin-dev.styletribute.com/inventory/products";

jest.setTimeout(40000);

beforeAll(async () => {
  await driver.get(login_url);
  await driver.wait(until.elementLocated(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")), 20000);
  await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(2) > input")).sendKeys("admin@styletribute.com");
  await driver.findElement(By.css("body > app-root > app-auth > app-login > div > div > div > section > form > div:nth-child(3) > input")).sendKeys("secret", Key.RETURN);
  await driver.wait(
    until.elementLocated(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > input")),
    10000
  );
});

beforeEach(async () => {
  await driver.get(product_page_url);
  await driver.wait(
    until.elementLocated(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > input")),
    10000
  );
});

describe("Testcase inventory", async () => {
  //     it("able to search by SKU", async () => {
  //       await driver
  //         .findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > input"))
  //         .sendKeys("SVISBG14503", Key.RETURN);
  //       await driver.sleep(2000);
  //       let text = await driver.findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(4) > table > tbody > tr > td:nth-child(3)")).getText();
  //       expect("SVISBG14503").toEqual(text);
  //     });

  //   it("able to search by multi SKU", async () => {
  //     let skus = ["SVMMBG14500", "SVSGCL14535"];
  //     let keyword = skus.join(",");
  //     await driver.wait(
  //       until.elementLocated(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > input")),
  //       10000
  //     );
  //     await driver
  //       .findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > input"))
  //       .sendKeys(keyword, Key.RETURN);
  //     await driver.sleep(2000);
  //     let flag = true;

  //     for (const k in skus) {
  //       if (!Contain(driver, By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(4) > table > tbody"), skus[k])) {
  //         flag = false;
  //       }
  //     }
  //     expect(flag).toBeTruthy();
  //   });
  it("able to add existing product to inventory in product page", async () => {
    let mock_sku = "SDSGAC21719";
    await driver
      .findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > input"))
      .sendKeys(mock_sku, Key.RETURN);
    await driver.sleep(2000);
    let can_see_add_new_product_section = await driver.findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div.row.pending-products")).isDisplayed();
    expect(can_see_add_new_product_section).toBeTruthy();

    // let text2 = await driver.findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div.row.pending-products > table > tbody > tr > td:nth-child(1)")).getText();
    // expect(mock_user.SKU).toEqual(text2);
    // await driver.findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div.row.pending-products > table > tbody > tr > td:nth-child(3) > select")).sendKeys("SG1");
    // await driver
    //   .findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div.row.pending-products > table > tbody > tr > td:nth-child(4) > select"))
    //   .sendKeys("RACK 1-2");
    // await driver.findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div.row.pending-products > table > tbody > tr > td:nth-child(5) > button")).click();
  });
  //   it("search keyword_Damier", async () => {
  //     await driver.wait(
  //       until.elementLocated(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > div > button")),
  //       10000
  //     );
  //     await driver.sleep(3000);
  //     await driver
  //       .findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > div > button"))
  //       .click();
  //     await driver.sleep(3000);
  //     await driver
  //       .findElement(
  //         By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > div > ul > li:nth-child(2) > a")
  //       )
  //       .click();
  //     await driver.sleep(3000);
  //     await driver.wait(
  //       until.elementLocated(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > div")),
  //       10000
  //     );
  //     await driver
  //       .findElement(By.css("#layout > div.right_col > app-inventory > app-list > div > div > div > div:nth-child(2) > app-multi-purpose-search > form > div:nth-child(2) > div > input"))
  //       .sendKeys("Damier", Key.RETURN);
  //   });
  //   it("test add new destination successfully_destination types is created", async () => {
  //     await driver.switchTo(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(5) > a"));
  //     await driver.wait(until.elementLocated(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(5) > a")), 10000);
  //     await driver.sleep(2000);
  //     await driver.findElement(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(5) > a")).click();
  //     await driver.wait(
  //       until.elementLocated(By.css("#layout > div.right_col > app-destination > app-list > div > div > div > div:nth-child(2) > div > div > div.col-right.search.text-right > a")),
  //       10000
  //     );
  //     await driver.findElement(By.css("#layout > div.right_col > app-destination > app-list > div > div > div > div:nth-child(2) > div > div > div.col-right.search.text-right > a")).click();
  //     await driver.wait(until.elementLocated(By.css("#layout > div.right_col > app-destination > app-create > div > div.x_content > div > form > div:nth-child(1) > select")), 10000);
  //     await driver.findElement(By.css("#layout > div.right_col > app-destination > app-create > div > div.x_content > div > form > div:nth-child(1) > select")).sendKeys("WAREHOUSE");
  //     await driver.sleep(2000);
  //     await driver.findElement(By.css("#layout > div.right_col > app-destination > app-create > div > div.x_content > div > form > div:nth-child(2) > input")).sendKeys("Dtest");
  //     await driver.findElement(By.css("#layout > div.right_col > app-destination > app-create > div > div.x_content > div > form > div:nth-child(3) > button")).click();
  //   });
  //   it("test deleted created destination", async () => {
  //     await driver.wait(until.elementLocated(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(5) > a")), 10000);
  //     await driver.sleep(2000);
  //     await driver.findElement(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(5) > a")).click();
  //     await driver.wait(
  //       until.elementLocated(By.css("#layout > div.right_col > app-destination > app-list > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(4) > button")),
  //       10000
  //     );
  //     await driver.findElement(By.css("#layout > div.right_col > app-destination > app-list > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(4) > button")).click();
  //     await driver.wait(until.elementLocated(By.css("#swal2-content")), 10000);
  //     let text = await driver.findElement(By.css("#swal2-content")).getText();
  //     expect("Do you want to delete this Destination?").toEqual(text);
  //     await driver.findElement(By.css("body > div.swal2-container.swal2-center.swal2-fade.swal2-shown > div > div.swal2-actions > button.swal2-confirm.swal2-styled")).click();
  //   });
  //   it("Create location for created destination successfully", async () => {
  //     await driver.wait(until.elementLocated(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(6) > a")), 10000);
  //     await driver.sleep(2000);
  //     await driver.findElement(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(6) > a")).click();
  //     await driver.wait(
  //       until.elementLocated(By.css("#layout > div.right_col > app-location > app-list > div > div > div.dataTables_wrapper > div:nth-child(2) > div > div > div.col-right.search.text-right")),
  //       10000
  //     );
  //     await driver.findElement(By.css("#layout > div.right_col > app-location > app-list > div > div > div.dataTables_wrapper > div:nth-child(2) > div > div > div.col-right.search.text-right")).click();
  //     await driver.findElement(By.css("#layout > div.right_col > app-location > app-create > div > div.x_content > div > form > div:nth-child(1) > input")).sendKeys("Rack Dtest");
  //     await driver.findElement(By.css("#layout > div.right_col > app-location > app-create > div > div.x_content > div > form > div:nth-child(2) > input")).sendKeys("Rack Dtest");
  //     await driver.findElement(By.css("#layout > div.right_col > app-location > app-create > div > div.x_content > div > form > div:nth-child(3) > input")).sendKeys("Clothes");
  //     await driver.findElement(By.css("#layout > div.right_col > app-location > app-create > div > div.x_content > div > form > div:nth-child(4) > select")).sendKeys("Dtest");
  //     await driver.findElement(By.css("#layout > div.right_col > app-location > app-create > div > div.x_content > div > form > div:nth-child(5) > button")).click();
  //   });
  //     it("Delete location", async() => {
  //         let i = 1;
  //         await driver.wait(until.elementLocated(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(6) > a")), 10000);
  //         await driver.sleep(2000);
  //         await driver.findElement(By.css("#sidebar-menu > div > ul > li.active > ul > li:nth-child(6) > a")).click();
  //         for( i ; i < 6; i++)
  //         {
  //             let text = await driver.findElement(By.css("#layout > div.right_col > app-location > app-list > div > div > div.dataTables_wrapper > div:nth-child(2) > table > tbody > tr:nth-child(${i}) > td:nth-child(1)")).getText();
  //             if (text == "Rack Dtest")
  //             {
  //                 await driver.findElement(By.css("#layout > div.right_col > app-location > app-list > div > div > div.dataTables_wrapper > div:nth-child(2) > table > tbody > tr:nth-child(${i}) > td:nth-child(5) > button")).click();
  //             }
  //         }
  //     })
});
