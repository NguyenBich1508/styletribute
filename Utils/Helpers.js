/**
 * 
 * @param {*} driver Selenium driver instance
 * @param {*} selector 
 * @param {*} text 
 */
export const Contain = async (driver, selector, text) => {
  let data = await driver.findElement(selector).getText();
  let pattern = new RegExp(`${text}`);
  let is_containt_data = pattern.test(data);
  return is_containt_data;
};
